package common_engine

import "time"

type Command interface {
	GetCommandParamsOptions() []CommandParamsOption
	GetCommandText() string
	GetPriority() float64
	Handler(*TaskCommand, map[string]CommandResult) (interface{}, error)
	GetCommandOption() *CommandOption
}

type CommandOption struct {
	Timeout     time.Duration
	DelayTime   time.Duration
	RetryTime   int
	Must        *bool // must need params option, nil: ignore ,true: need ,false don't need
	StartRange  int // specify params range, if StartRange equal 0,it will be ignore
	ReferResult []string // refer other command handler result
	FilterFunc  Filter
}

func NewCommandOption() *CommandOption {
	return &CommandOption{
		Timeout:   2 * time.Second,
		DelayTime: 0,
		RetryTime: 0,
	}
}

type ParamsType int64

const (
	String ParamsType = 1
	Int    ParamsType = 2
	Float  ParamsType = 3
	Json   ParamsType = 4
	Link   ParamsType = 5
	Number ParamsType = 6
	File   ParamsType = 7
	Dir    ParamsType = 8
)

type CommandParamsOption struct {
	ParamsType ParamsType
	Must       bool
	Multi      bool // is multi params
	MultiNum   int
	Suffix     *string
	Prefix     *string
	Include    *string
	Exclude    *string
	Priority   int64
}