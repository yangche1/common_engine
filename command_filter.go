package common_engine

import (
	"context"
	"gitlab.com/yangche1/common_engine/common/status"
	"strings"
)

const (
	DysonPrefix = "dyson"
)

type dysonFilter struct {
}

const (
	SplitChar = " "
)

func (f *dysonFilter) Filter(ctx context.Context, task CommandTask) (bool, error) {
	text := strings.Trim(task.GetCommandsText(), SplitChar)
	if len(text) == 0 {
		return true, status.NewErr(status.CommandTextIsEmpty, nil)
	}
	
	if !strings.HasPrefix(text, DysonPrefix) {
		return true, nil
	}
	return false, nil
}