package common_engine

type Task struct {
	text      string
	timestamp int64
	extend    string
	extendInfo map[string]string
	option    *TaskOption
}

func (t *Task) GetCommandsText() string {
	return t.text
}

func (t *Task) GetExtendInfo() map[string]string {
	return t.extendInfo
}

func (t *Task) GetTaskOption() *TaskOption {
	return t.option
}

type TaskCommand struct {
	Text   string
	Params []*TaskCommand // param's params is empty
	Index  int
	Val    interface{}
	Err    error
}