package common_engine

import (
	"errors"
	"gitlab.com/yangche1/common_engine/common/status"
)

type Response interface {
	GetResult() (interface{}, error)
	GetStatusCode() int64
}

type defaultResp struct {
	err  error
	res  interface{}
	call Call
}

func NewResponse() Response {
	return &defaultResp{
		err:  nil,
		res:  nil,
		call: nil,
	}
}

func (d *defaultResp) GetResult() (interface{}, error) {
	if d.call != nil {
		val, err := d.call.Wait()
		d.res = val
		d.err = err
	}
	return d.res, d.err
}

func (d *defaultResp) GetStatusCode() int64 {
	if d.err == nil {
		return 0
	}
	var st *status.ErrInfo
	if errors.As(d.err, &st) {
		return st.GetCode()
	}
	return status.UnKnowInnerErr.Code
}