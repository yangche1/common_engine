package common_engine

import (
	"context"
)

type Handler func(ctx context.Context, originCommand string, command, param string) error

type Engine interface {
	Process
	BindCommand(command Command) Engine
	Exec(command CommandTask) Response
	Err() error
}

type Process interface {
	Prepare() error
	Running() error
	Release() error
}

type EngineOption struct {
	filterFunc   Filter
	transferFunc Transfer
	poolSize     int
	queueSize    int
}

func (e *syncEngine) WithEngineOptions(options ...EngineOption) error {
	if len(options) == 0 {
		options = append(options, EngineOption{
			poolSize:  10,
			queueSize: 10,
		})
	}
	
	option := options[0]
	if option.filterFunc != nil {
		e.filterFunc = option.filterFunc
	}
	if option.transferFunc != nil {
		e.transfer = option.transferFunc
	}
	
	if option.poolSize != 0 {
		p, err := NewPool(option.poolSize)
		if err != nil {
			return err
		}
		e.pool = p
	}
	
	if option.queueSize != 0 {
		e.queue = make(chan CommandTask, option.queueSize)
	}
	return nil
}