package main

import (
	"fmt"
	"gitlab.com/yangche1/common_engine"
)

type DysonCommand struct {
}

func (d *DysonCommand) GetCommandParamsOptions() []common_engine.CommandParamsOption {
	return []common_engine.CommandParamsOption{}
}

func (d *DysonCommand) GetCommandText() string {
	return "dyson"
}

func (d *DysonCommand) GetPriority() float64 {
	return 1
}

func (d *DysonCommand) Handler(cmd *common_engine.TaskCommand, commandRes map[string]common_engine.CommandResult) (interface{}, error) {
	fmt.Println("dyson command is here")
	return cmd.Text, nil
}

func (d *DysonCommand) GetCommandOption() *common_engine.CommandOption {
	must := true
	return &common_engine.CommandOption{
		Must: &must,
	}
}