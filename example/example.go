package main

import (
	"context"
	"fmt"
	"gitlab.com/yangche1/common_engine"
)

func main() {
	ctx := context.Background()
	e := common_engine.NewEngine(ctx, 100)
	err := e.BindCommand(&DysonCommand{}).BindCommand(&DysonSum{}).Err()
	if err != nil {
		panic(err)
	}
	go func() {
		resp := e.Exec(&DysonTask{})
		val, err := resp.GetResult()
		fmt.Println(fmt.Sprintf("process finish val=%v,err=%v", val, err))
	}()
	if err = e.Running(); err != nil {
		fmt.Println(fmt.Sprintf("engine running fail err=%v", err))
	}
}