package main

import "gitlab.com/yangche1/common_engine"

type DysonTask struct {
}

func (t *DysonTask) GetCommandsText() string {
	return "dyson sum -2 2 10 10"
}

func (t *DysonTask) GetExtendInfo() map[string]string {
	return nil
}

func (t *DysonTask) GetTaskOption() *common_engine.TaskOption {
	return &common_engine.TaskOption{}
}