package main

import (
	"gitlab.com/yangche1/common_engine"
)

type DysonSum struct {
}

func (d *DysonSum) GetCommandParamsOptions() []common_engine.CommandParamsOption {
	return []common_engine.CommandParamsOption{
		{
			ParamsType: common_engine.Int,
			Must:       true,
			Priority:   1,
		},
	}
}

func (d *DysonSum) GetCommandText() string {
	return "sum"
}

func (d *DysonSum) GetPriority() float64 {
	return 1
}

func (d *DysonSum) Handler(cmd *common_engine.TaskCommand, commandRes map[string]common_engine.CommandResult) (interface{}, error) {
	sum := int64(0)
	for _, params := range cmd.Params {
		sum += params.Val.(int64)
	}
	return sum, nil
}

func (d *DysonSum) GetCommandOption() *common_engine.CommandOption {
	must := true
	return &common_engine.CommandOption{
		Must: &must,
	}
}