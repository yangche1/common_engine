package common_engine

import "context"

type Filter interface {
	Filter(ctx context.Context, task CommandTask) (bool, error)
}