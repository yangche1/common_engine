package common_engine

type CommandResult interface {
	GetResult() (interface{}, error)
	GetCommandText() string
}

type cmdResult struct {
	val  interface{}
	err  error
	text string
}

func (c *cmdResult) GetResult() (interface{}, error) {
	return c.val, c.err
}

func (c *cmdResult) GetCommandText() string {
	return c.text
}