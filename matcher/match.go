package matcher

type ExceptType int64

const (
	ExceptString ExceptType = 1
	ExceptInt    ExceptType = 2
	ExceptFloat  ExceptType = 3
	ExceptJson   ExceptType = 4
	ExceptLink   ExceptType = 5
	ExceptNumber ExceptType = 6
	ExceptFile   ExceptType = 7
	ExceptDir    ExceptType = 8
)

type Match interface {
	Match(text interface{}, typ ExceptType) bool
	Transfer(text interface{}, typ ExceptType) (interface{}, error)
}