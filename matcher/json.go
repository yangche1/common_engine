package matcher

import (
	"encoding/json"
	"fmt"
	"gitlab.com/yangche1/common_engine/common/status"
)

func init() {
	RegisterMatcher(ExceptJson, &JsonMatcher{})
}

type JsonMatcher struct {
}

func (j *JsonMatcher) Match(text interface{}, typ ExceptType) bool {
	if text == nil {
		return false
	}
	return json.Valid([]byte(fmt.Sprintf("%v", text)))
}

func (j *JsonMatcher) Transfer(text interface{}, typ ExceptType) (interface{}, error) {
	if typ != ExceptJson {
		return nil, status.NewErr(status.UnSupportMatchType, nil)
	}
	switch text.(type) {
	case string:
	default:
		return nil, status.NewErr(status.IllegalJsonText, nil)
	}
	return text, nil
}