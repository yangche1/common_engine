package matcher

import (
	"encoding/json"
	"gitlab.com/yangche1/common_engine/common/status"
)

func init() {
	RegisterMatcher(ExceptString, &StringMatch{})
}

type StringMatch struct {
}

func (s *StringMatch) Match(_ interface{}, _ ExceptType) bool {
	return true
}

func (s *StringMatch) Transfer(text interface{}, typ ExceptType) (interface{}, error) {
	if text == nil {
		return nil, status.NewErr(status.IllegalParams, nil)
	}
	bytes, err := json.Marshal(text)
	if err != nil {
		return nil, status.NewErr(status.IllegalPathText, err)
	}
	return string(bytes), nil
}