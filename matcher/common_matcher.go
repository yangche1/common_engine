package matcher

var matchHandler = map[ExceptType]Match{}

func RegisterMatcher(typ ExceptType, f Match) {
	matchHandler[typ] = f
}

func GetMatchers() map[ExceptType]Match {
	return matchHandler
}