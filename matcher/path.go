package matcher

import (
	"fmt"
	"gitlab.com/yangche1/common_engine/common/status"
	"io/fs"
	"os"
)

func init() {
	matcher := &PathMatcher{}
	RegisterMatcher(ExceptDir, matcher)
	RegisterMatcher(ExceptFile, matcher)
}

type PathMatcher struct {
}

func (p *PathMatcher) Match(text interface{}, typ ExceptType) bool {
	str := fmt.Sprintf("%v", text)
	switch typ {
	case ExceptDir:
		if !fs.ValidPath(str) {
			return false
		}
		f, err := os.Stat(str)
		if err != nil {
			return false
		}
		return f.IsDir()
	case ExceptFile:
		if !fs.ValidPath(str) {
			return false
		}
		f, err := os.Stat(str)
		if err != nil {
			return false
		}
		return !f.IsDir()
	}
	return false
}

func (p *PathMatcher) Transfer(text interface{}, typ ExceptType) (interface{}, error) {
	return nil, status.NewErr(status.UnSupportOperation, nil)
}