package matcher

import (
	"fmt"
	"gitlab.com/yangche1/common_engine/common/status"
	"net/url"
	"strings"
)

func init() {
	RegisterMatcher(ExceptLink, &HttpMatcher{})
}

type HttpMatcher struct {
}

func (h *HttpMatcher) Match(text interface{}, typ ExceptType) bool {
	if typ != ExceptLink {
		return false
	}
	str := fmt.Sprintf("%v", text)
	return strings.HasPrefix(str, "http://") || strings.HasPrefix(str, "https://")
}

func (h *HttpMatcher) Transfer(text interface{}, typ ExceptType) (interface{}, error) {
	if typ != ExceptLink {
		return nil, status.NewErr(status.UnSupportMatchType, nil)
	}
	str := fmt.Sprintf("%v", text)
	var err error
	var target *url.URL
	target, err = url.Parse(str)
	return target, status.NewErr(status.IllegalHttpText, err)
	
}