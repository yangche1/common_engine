package matcher

import (
	"fmt"
	"gitlab.com/yangche1/common_engine/common/status"
	"strconv"
	"strings"
)

func init() {
	matcher := &NumberMatch{}
	RegisterMatcher(ExceptInt, matcher)
	RegisterMatcher(ExceptFloat, matcher)
	RegisterMatcher(ExceptNumber, matcher)
}

type NumberMatch struct {
}

func (s *NumberMatch) Match(text interface{}, typ ExceptType) bool {
	str := fmt.Sprintf("%v", text)
	switch typ {
	case ExceptInt:
		return s.IsInt(str)
	case ExceptFloat:
		return s.IsFloat(str)
	case ExceptNumber:
		return s.IsFloat(str) || s.IsInt(str)
	}
	return false
}

func (s *NumberMatch) IsInt(text string) bool {
	if text == "" {
		return false
	}
	chars := []rune(text)
	if chars[0] == '-' {
		chars = chars[1:]
	}
	for _, ch := range chars {
		if ch < '0' || ch > '9' {
			return false
		}
	}
	return true
}

func (s *NumberMatch) IsFloat(text string) bool {
	if text == "" {
		return false
	}
	// only trim once
	text = strings.TrimPrefix(text, "-")
	arr := strings.Split(text, ".")
	if len(arr) != 2 {
		return false
	}
	for _, t := range arr {
		if len(t) == 0 {
			return false
		}
		for _, ch := range []rune(t) {
			//TODO judge like 1.2e10 string
			if ch < '0' || ch > '9' {
				return false
			}
		}
	}
	return true
}

func (s *NumberMatch) Transfer(text interface{}, typ ExceptType) (interface{}, error) {
	str := fmt.Sprintf("%v", text)
	switch typ {
	case ExceptInt, ExceptFloat, ExceptNumber:
		if s.IsInt(str) {
			v, err := strconv.ParseInt(str, 10, 64)
			if err != nil {
				return nil, status.NewErr(status.IllegalNumberText, err)
			}
			return v, nil
		}
		v, err := strconv.ParseFloat(str, 64)
		if err != nil {
			return nil, status.NewErr(status.IllegalNumberText, err)
		}
		return v, nil
	}
	return nil, status.NewErr(status.UnSupportMatchType, nil)
}