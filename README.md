# Common_Engine

> common_engine 设计之初是一个通用化的命令执行引擎。可以通用化、支持自定义地
> 解析一些命令
> 

# 设计
> ![内部流程图](./doc/common_engine.png '')
> 

# 使用示例

``` go
package main

import (
	"common_engine"
	"context"
	"fmt"
	"time"
)

func main() {
	ctx := context.Background()
	e := common_engine.NewEngine(ctx, 10)
	// need implement Command interface
	err := e.BindCommand(&DysonCommand{}).Err()
	if err != nil {
		panic(err)
	}
	// send a task
	go func() {
		// need implement TaskCommand interface
		resp := e.Exec(&DysonTask{})
		val, err := resp.GetResult()
		fmt.Println(fmt.Sprintf("process success val=%v,err=%v", val, err))
	}()
	// do running
	if err = e.Running(); err != nil {
		fmt.Println(fmt.Sprintf("engine running fail err=%v", err))
	}
}
```