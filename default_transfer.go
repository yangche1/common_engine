package common_engine

import (
	"context"
	"gitlab.com/yangche1/common_engine/common/status"
	"gitlab.com/yangche1/common_engine/matcher"
	"math"
	"regexp"
	"sort"
	"strings"
)

type defaultTransfer struct {
	matcher map[matcher.ExceptType]matcher.Match
}

func (t *defaultTransfer) DoParse(ctx context.Context, item CommandTask) ([]*TaskPart, error) {
	ta := item.(*innerTask)
	commandText := ta.GetCommandsText()
	regx, err := regexp.Compile("/s+")
	if err != nil {
		return nil, status.NewErr(status.InnerError, err)
	}
	bytes := regx.ReplaceAll([]byte(commandText), []byte(" "))
	commandArr := strings.Split(string(bytes), " ")
	if len(commandArr) == 0 {
		return nil, status.NewErr(status.IllegalCommandInfo, nil)
	}
	return t.toTaskPart(commandArr)
}

func (t *defaultTransfer) DoMatch(ctx context.Context, commands map[string]Command, taskInfo []*TaskPart) ([]*TaskCommand, error) {
	if len(commands) == 0 || len(taskInfo) == 0 {
		return nil, status.NewErr(status.IllegalCommandInfo, nil)
	}
	
	res := make([]*TaskCommand, 0)
	for i := 0; i < len(taskInfo); i++ {
		part := taskInfo[i]
		var taskCommand *TaskCommand
		command, match := commands[part.text]
		var cmdOption *CommandOption
		if match {
			cmdOption = command.GetCommandOption()
			part.typ = KeyWord
			// new task command
			taskCommand = &TaskCommand{
				Text:   part.text,
				Params: make([]*TaskCommand, 0),
				Index:  part.index,
			}
			
			// don't set params option ,just skip
			if cmdOption == nil {
				continue
			}
			
			// get params type
			paramsOptionList := command.GetCommandParamsOptions()
			if len(paramsOptionList) == 0 {
				continue
			}
			
			// params priority sort
			// command_sort by priority,if priority is same , must option will be before
			// if must is same, order by source index in arr
			sort.Sort(ParamsOptionArr(paramsOptionList))
			
			// TODO add option to ignore key word check
			// get max params match count
			paramsRange := math.MaxInt16
			if cmdOption.StartRange != 0 {
				paramsRange = cmdOption.StartRange
			}
			paramsCount := getParamsEnd(taskInfo, commands, int32(i), int32(paramsRange))
			mustCount := getMustParamsCount(paramsOptionList)
			if paramsCount < mustCount {
				return nil, status.NewErr(status.InputCommandIllegal, nil)
			}
			
			for _, option := range paramsOptionList {
				cmdParamsList := make([]*TaskCommand, 0)
				
				for j := 1; j <= paramsCount; j++ {
					var cmdParams *TaskCommand
					
					paramsPart := taskInfo[i+j]
					if paramsPart.typ != Default {
						continue
					}
					
					// is match
					expType := matcher.ExceptType(option.ParamsType)
					realMatcher, ok := t.matcher[expType]
					if !ok {
						return nil, status.NewErr(status.UnSupportExceptType, nil)
					}
					if realMatcher.Match(paramsPart.text, expType) {
						val, err := realMatcher.Transfer(paramsPart.text, expType)
						if err != nil {
							return nil, err
						}
						cmdParams = &TaskCommand{
							Text:   paramsPart.text,
							Params: nil,
							Index:  i + j,
						}
						if val != nil {
							cmdParams.Val = val
						}
						paramsPart.typ = Params
					}
					
					if cmdParams != nil {
						cmdParamsList = append(cmdParamsList, cmdParams)
					}
					
					if !option.Multi {
						break
					}
					
					if option.Multi && option.MultiNum != 0 && len(cmdParamsList) == option.MultiNum {
						break
					}
				}
				
				if len(cmdParamsList) == 0 {
					if option.Must {
						return nil, status.NewErr(status.MustParamsNotFound, nil)
					}
					continue
				}
				
				taskCommand.Params = append(taskCommand.Params, cmdParamsList...)
			}
		}
		
		if taskCommand != nil {
			res = append(res, taskCommand)
			i += len(taskCommand.Params)
		} else if cmdOption != nil && cmdOption.Must != nil && *cmdOption.Must {
			// don't match command but command is must
			return nil, status.NewErr(status.MustCommandNotFound, nil)
		}
	}
	return res, nil
}

func (t *defaultTransfer) toTaskPart(texts []string) ([]*TaskPart, error) {
	parts := make([]*TaskPart, 0, len(texts))
	for i, text := range texts {
		parts = append(parts, &TaskPart{
			text:  text,
			index: i,
			typ:   Default,
		})
	}
	return parts, nil
}

func getParamsEnd(taskPart []*TaskPart, commands map[string]Command, start, startRange int32) int {
	count := 0
	for i := start + 1; i <= start+startRange; i++ {
		if i >= int32(len(taskPart)) {
			break
		}
		// is key word
		if _, is := commands[taskPart[i].text]; !is {
			count++
		} else {
			break
		}
	}
	return count
}

func getMustParamsCount(options []CommandParamsOption) int {
	count := 0
	for _, option := range options {
		if option.Must {
			count++
		}
	}
	return count
}