package common_engine

import (
	"context"
	"gitlab.com/yangche1/common_engine/matcher"
)

type Transfer interface {
	DoParse(ctx context.Context, task CommandTask) ([]*TaskPart, error)
	DoMatch(ctx context.Context, commands map[string]Command, taskInfo []*TaskPart) ([]*TaskCommand, error)
}

func NewDefaultTransfer() Transfer {
	return &defaultTransfer{
		matcher: matcher.GetMatchers(),
	}
}