package common_engine

type CommandTask interface {
	GetCommandsText() string
	GetExtendInfo() map[string]string
	GetTaskOption() *TaskOption
}

type TaskOption struct {
	RetryTime int
}

func NewTask() CommandTask {
	return &Task{
		text:       "",
		timestamp:  0,
		extend:     "",
		extendInfo: nil,
		option:     &TaskOption{},
	}
}

type innerTask struct {
	taskType TaskType
	task     CommandTask
	call     Call
	status   ProcessCode
	val      interface{}
	err      error
	commands []*TaskCommand
}

type TaskType int64

var (
	SyncTask  TaskType = 1
	AsyncTask TaskType = 2
)

type ProcessCode int64

func (t *innerTask) GetCommandsText() string {
	return t.task.GetCommandsText()
}

func (t *innerTask) GetExtendInfo() map[string]string {
	return t.task.GetExtendInfo()
}

func (t *innerTask) GetTaskCommands() []TaskCommand {
	return nil
}

func (t *innerTask) GetTaskOption() *TaskOption {
	return t.task.GetTaskOption()
}

type PartType int64

var (
	Default PartType = 1
	Params  PartType = 2
	KeyWord PartType = 3
)

type TaskPart struct {
	text  string
	index int
	typ   PartType
}