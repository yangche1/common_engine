package common_engine

type CommandArr []Command

func (c CommandArr) Len() int {
	return len(c)
}

func (c CommandArr) Swap(i, j int) {
	c[i], c[j] = c[j], c[i]
}

func (c CommandArr) Less(i, j int) bool {
	if c[i].GetPriority() < c[j].GetPriority() {
		return true
	} else if i < j {
		return true
	}
	return false
}