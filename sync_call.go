package common_engine

import "sync"

type SyncCall struct {
	once sync.Once
	sig  chan struct{}
	err  error
	val  interface{}
}

func (s *SyncCall) Start(fn func() (interface{}, error)) {
	s.once.Do(func() {
		defer s.Done()
		val, err := fn()
		s.val = val
		s.err = err
	})
}

func (s *SyncCall) Done() {
	s.sig <- struct{}{}
}

func (s *SyncCall) Wait() (interface{}, error) {
	<-s.sig
	return s.val, s.err
}