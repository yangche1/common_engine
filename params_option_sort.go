package common_engine

type ParamsOptionArr []CommandParamsOption

func (p ParamsOptionArr) Len() int {
	return len(p)
}

func (p ParamsOptionArr) Swap(i, j int) {
	p[i], p[j] = p[j], p[i]
}

func (p ParamsOptionArr) Less(i, j int) bool {
	if p[i].Priority < p[j].Priority {
		return true
	} else if p[i].Priority == p[j].Priority {
		if p[i].Must {
			return true
		} else if p[j].Must {
			return false
		}
		if i < j {
			return true
		}
	}
	return false
}