package common_engine

import (
	"github.com/panjf2000/ants"
	"gitlab.com/yangche1/common_engine/common/constant"
)

type Pool interface {
	Size() int
	Submit(func()) error
	Close()
}

func NewPool(size int) (Pool, error) {
	options := ants.Options{
		ExpiryDuration:   constant.PoolOption.LiveTime,
		PreAlloc:         constant.PoolOption.LazyLoad,
		MaxBlockingTasks: constant.PoolOption.BlockingNum,
		Nonblocking:      !constant.PoolOption.Blocking,
		PanicHandler:     constant.PoolOption.PanicHandler,
	}
	option := ants.WithOptions(options)
	p, err := ants.NewPool(size, option)
	if err != nil {
		return nil, err
	}
	return &pool{
		pool: p,
	}, nil
}

type pool struct {
	pool *ants.Pool
}

func (p *pool) Submit(fn func()) error {
	err := p.pool.Submit(fn)
	if err != nil {
		return err
	}
	return nil
}

func (p *pool) Size() int {
	return p.pool.Cap()
}

func (p *pool) Close() {
	p.pool.Release()
}