package constant

import (
	"fmt"
	"time"
)

type poolOption struct {
	PoolSize     int
	Blocking     bool
	BlockingNum  int
	LiveTime     time.Duration
	LazyLoad     bool
	PanicHandler func(interface{})
}

var PoolOption = poolOption{
	PoolSize:    10,
	Blocking:    true,
	BlockingNum: 10,
	LiveTime:    10 * time.Second,
	LazyLoad:    true,
	PanicHandler: func(err interface{}) {
		if err != nil {
			e := fmt.Sprintf("%v", err)
			fmt.Printf("%v\n", e)
		}
	},
}