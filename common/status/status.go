package status

type Status struct {
	Code int64
	Msg  string
}

func newStatus(code int64, msg string) Status {
	return Status{
		Code: code,
		Msg:  msg,
	}
}

var (
	UnSupportOperation  = newStatus(100001, "Un Support Operation")
	UnSupportTask       = newStatus(100002, "Un Support Task")
	UnSupportExceptType = newStatus(100003, "Un Support Except Type")
	CommandTextIsEmpty  = newStatus(400001, "Command Text Is Empty")
	TaskTypeIllegal     = newStatus(400002, "Un Support Task Type")
	UnSupportMatchType  = newStatus(400003, "Un Support Match Type")
	IllegalParams       = newStatus(400004, "Params Illegal")
	IllegalCommandInfo  = newStatus(400005, "Illegal Command Info")
	InputCommandIllegal = newStatus(400006, "Input Command Illegal")
	MustParamsNotFound  = newStatus(400007, "Must Params Not Found")
	MustCommandNotFound = newStatus(400008, "Must Command Not Found")
	TransferTextIsEmpty = newStatus(400008, "Transfer Text Is Empty")
	IllegalHttpText     = newStatus(4010001, "Illegal Http Text")
	IllegalJsonText     = newStatus(401002, "Illegal Json Text")
	IllegalNumberText   = newStatus(401003, "Illegal Number Text")
	IllegalPathText     = newStatus(402004, "Illegal Path Text")
	InnerError          = newStatus(500001, "Server Inner Error")
	UnKnowInnerErr      = newStatus(-1, "Un Know Inner Err")
)