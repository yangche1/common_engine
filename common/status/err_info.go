package status

import "fmt"

type ErrInfo struct {
	err  error
	code int64
	msg  string
}

func (s *ErrInfo) Error() string {
	return fmt.Sprintf("process fail [code=%v,msg=%v,err=%v,]", s.code, s.msg, s.err)
}

func (s *ErrInfo) GetCode() int64 {
	return s.code
}

func (e *ErrInfo) GetMsg() string {
	return e.msg
}

func NewErr(s Status, err error) *ErrInfo {
	return &ErrInfo{
		err:  err,
		code: s.Code,
		msg:  s.Msg,
	}
}