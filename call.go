package common_engine

import (
	"sync"
)

type Call interface {
	Start(func() (interface{}, error))
	Done()
	Wait() (interface{}, error)
}

func NewSyncCall() Call {
	return &SyncCall{
		once: sync.Once{},
		sig:  make(chan struct{}),
	}
}